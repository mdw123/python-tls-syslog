===============================
Python Syslog over TCP/TLS
===============================

|  |build| |license| |kit| |format|

This library allows sending syslog messages over TCP and TLS, similar to how Python's built-in `SysLogHandler <https://docs.python.org/3/library/logging.handlers.html#sysloghandler>`_ sends log lines over UDP. Since TCP isn't fire-and-forget like UDP, this library uses a daemon thread to send log-lines in the background without blocking the main application thread. Shutdown of the main process, however, is blocked until all log lines in the send-queue have been sent.


Installation
============

The documentation below assumes you're configuring the library to send logging to `Papertrail <https://papertrailapp.com/>`_, since Papertrail is a commonly used rsyslog provider that supports TCP/TLS connections. The same instructions should be applicable to any TCP/TLS syslog listener.


Obtain the TLS CA Certificates
------------------------------

Download the syslog listener's `TLS certificates file <https://help.papertrailapp.com/kb/configuration/encrypting-remote-syslog-with-tls-ssl/#download-root-certificates>`_ in PEM format and save it somewhere. For example:

.. code:: bash

    curl -o /path/to/papertrail-bundle.pem https://papertrailapp.com/tools/papertrail-bundle.pem

This step isn't needed is you aren't planning to validate the listener's certificate, but you should always validate the certificate. Otherwise, you might as well continue using syslog over UDP.


Setup: Django
-------------

The below sample code, when placed in your project's ``settings.py`` file, configures Django's logging framework.

.. code:: python

    import ssl

    syslog_host = 'logsX.papertrailapp.com'
    syslog_port = 55555
    syslog_cert_path = '/path/to/papertrail-bundle.pem'

    LOGGING = {
        'version': 1,
        'formatters': {
            'simple': {
                'format': '%(asctime)s django %(name)s: %(levelname)s %(message)s',
                'datefmt': '%Y-%m-%dT%H:%M:%S',
            },
        },
        'handlers': {
            'syslog': {
                'level': 'INFO',
                'class': 'tlssyslog.handlers.TLSSysLogHandler',
                'formatter': 'simple',
                'address': (syslog_host, syslog_port),
                'ssl_kwargs': {
                    'cert_reqs': ssl.CERT_REQUIRED,
                    'ssl_version': ssl.PROTOCOL_TLS,
                    'ca_certs': syslog_cert_path,
                },
            },
        },
        'root': {
            'handlers': ['syslog'],
            'level': 'INFO',
        }
    }


Setup: Generic Python Application
---------------------------------

The below sample code configures Python's logging framework.

.. code:: python

    import logging.config
    import ssl

    syslog_host = 'logsX.papertrailapp.com'
    syslog_port = 55555
    syslog_cert_path = '/path/to/papertrail-bundle.pem'

    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'simple': {
                'format': '%(asctime)s django %(name)s: %(levelname)s %(message)s',
                'datefmt': '%Y-%m-%dT%H:%M:%S',
            },
        },
        'handlers': {
            'syslog': {
                'level': 'INFO',
                'class': 'tlssyslog.handlers.TLSSysLogHandler',
                'formatter': 'simple',
                'address': (syslog_host, syslog_port),
                'ssl_kwargs': {
                    'cert_reqs': ssl.CERT_REQUIRED,
                    'ssl_version': ssl.PROTOCOL_TLS,
                    'ca_certs': syslog_cert_path,
                },
            },
        },
        'root': {
            'handlers': ['syslog'],
            'level': 'INFO',
        }
    })


Changelog
=========

0.2.0
------------------
- Add support for Django 2.1
- Add support for Python 3.7
- Migrate from Sentry's old SDK (raven) to their new SDK (sentry-sdk).

0.1.3
------------------
- Improves durability by better handling error conditions involving OSErrors thrown when opening the syslog collector socket.

0.1.2
------------------
- Fix issue where log lines weren't always completely sent over the socket connection, causing them to be lost.
- Adds better error handling via [Raven/Sentry](https://docs.sentry.io/clients/python/) (if installed) upon error in the socket send worker thread.

0.1.1
------------------
- Fix issue with forked processes not being able to log due to process copy-on-write behavior.
    - After a process fork occurs, the previously created queue and daemon thread are invalidated and recreated (for the child process only).
    - Uses the process PID to detect when a fork has occurred.

0.1.0
------------------
- Initial release.


.. |build| image:: https://gitlab.com/thelabnyc/python-tls-syslog/badges/master/build.svg
    :target: https://gitlab.com/thelabnyc/python-tls-syslog/commits/master
.. |license| image:: https://img.shields.io/pypi/l/python-tls-syslog.svg
    :target: https://pypi.python.org/pypi/
.. |kit| image:: https://badge.fury.io/py/python-tls-syslog.svg
    :target: https://pypi.python.org/pypi/python-tls-syslog
.. |format| image:: https://img.shields.io/pypi/format/python-tls-syslog.svg
    :target: https://pypi.python.org/pypi/python-tls-syslog
